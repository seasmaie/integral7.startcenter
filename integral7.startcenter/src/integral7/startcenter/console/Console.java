package integral7.startcenter.console;

import java.io.IOException;
import java.io.OutputStream;

import javafx.scene.control.TextArea;

public class Console extends OutputStream {

    private TextArea output;
    
	public Console(TextArea output){
        this.output = output;
    }
	  
	@Override
	public void write(int arg0) throws IOException {
		output.appendText(String.valueOf((char) arg0));
	}

}
