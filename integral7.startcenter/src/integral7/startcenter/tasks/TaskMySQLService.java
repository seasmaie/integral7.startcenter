package integral7.startcenter.tasks;

import integral7.startcenter.utils.Utils;
import javafx.concurrent.Task;

public class TaskMySQLService extends Task{

	private boolean start;
	private String mySQLServicename;
	
	public TaskMySQLService(boolean start, String mySQLServicename) {
		this.start = start;
		this.mySQLServicename = mySQLServicename;
	}
	@Override
	protected Object call() throws Exception {
		boolean ok = false;
		if(start) {
			System.out.println("Starte MySQL Dienst");
			Utils.startServiceMySQL();
			int counter = 1;
			do {
				ok = Utils.isRunningMySQL(mySQLServicename);
				counter++;
				Thread.sleep(1000);
			}
			while(!ok && counter < 8);			
		}
		else {
			System.out.println("Beende MySQL Dienst");
			Utils.stopServiceMySQL();
			int counter = 1;
			do {
				ok = Utils.isRunningMySQL(mySQLServicename);
				counter++;
				Thread.sleep(700);
			}
			while(!ok && counter < 8);	
		}
		
		return ok;
	}

}
