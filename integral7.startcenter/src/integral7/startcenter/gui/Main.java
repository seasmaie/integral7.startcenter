package integral7.startcenter.gui;
	
import java.io.PrintStream;

import integral7.startcenter.console.Console;
import integral7.startcenter.tasks.TaskMySQLService;
import integral7.startcenter.utils.Utils;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Main extends Application {
	
	private  Image imageHeader = new Image("/Header_Integra7Startcenter.png", 547, 117, true, true);
	private ImageView imageViewHeader = new ImageView(imageHeader);
        
    
	private Image imageOff = new Image("/off.png", 25, 25, true, true);
	private ImageView imageViewOff = new ImageView(imageOff);
	
	private Image imageOn = new Image("/on.png", 25, 25, true, true);
	private ImageView imageViewOn = new ImageView(imageOn);
	
	private Image updates = new Image("/update.png", 25, 25, true, true);
	private ImageView imageViewUpdate = new ImageView(updates);
//	
	private Image noUpdates = new Image("/no_update.png", 25, 25, true, true);
	private ImageView imageViewNoUpdate = new ImageView(noUpdates);
	
	private boolean start = true;
	
	private String integral7Exe = "integral.exe";
	private String integral7Path = "E:\\Integral_x64_131104\\";
	private String integral7UpdatePath;
	
	private String mySQLSeriveName = "MySQL";
	
	private static String PROP_UPDATE_DIR = "Integral7_update_dir";
	private static String PROP_I7_DIR = "Integral7_dir";
	private static String PROP_I7_EXE = "Integral7_exe";
	private static String PROP_MY_SQL_SERVICE = "MySQL_Dienstname";
	
	private Button bI7;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			start = !isRunning();
			
			initProperties();			
			StackPane topPane = new StackPane(imageViewHeader);
	        StackPane bottomPane = new StackPane();

	        VBox vBox = new VBox();
	        bottomPane.getChildren().add(vBox);
	        
	        HBox hBoxIntegral7 = new HBox(5);
	        hBoxIntegral7.setPadding(new Insets(5, 5, 5, 5));
	        bI7 = new Button();

		    bI7.setGraphic(!start ? imageViewOn : imageViewOff);

	        hBoxIntegral7.getChildren().add(bI7);

	        Button bUpdate = new Button();
	        
	        Image image2 = new Image("/no_update.png", 25, 25, true, true);
	        ImageView imageView2 = new ImageView(image2);
		    
		    bUpdate.setGraphic(imageView2);
		    
		    hBoxIntegral7.getChildren().add(bUpdate);
	        
	        vBox.getChildren().add(hBoxIntegral7);

	        
	        vBox.getChildren().add(new Label("Console"));
	        
	        TextArea ta = new TextArea();
	        Console console = new Console(ta);
	        PrintStream ps = new PrintStream(console, true);
	        System.setOut(ps);
	        System.setErr(ps);
	        
	        vBox.getChildren().add(ta);
	        
	        SplitPane splitPane = new SplitPane();
	        splitPane.setOrientation(Orientation.VERTICAL);
	        splitPane.getItems().addAll(topPane, bottomPane);
	        splitPane.setDividerPositions(0.25);
	        
	        

	        primaryStage.setScene(new Scene(new BorderPane(splitPane), 545, 350));
	        primaryStage.setTitle("Integral 7 Startcenter");
	        primaryStage.setResizable(false);
	        primaryStage.show();
	        
	        splitPane.lookupAll(".split-pane-divider").stream().forEach(div ->  div.setMouseTransparent(true));
	        
	        
	        bI7.setOnAction(new EventHandler<ActionEvent>() {
				
				@SuppressWarnings("unchecked")
				@Override
				public void handle(ActionEvent event) {			
					boolean gestartetMySQL = Utils.isRunningMySQL(mySQLSeriveName);
					boolean gestartetIntegral7 = Utils.isProcessRunning(integral7Exe);
										
					if(start) {
						if(!gestartetMySQL) {
							TaskMySQLService taskMxSQL = new TaskMySQLService(start, mySQLSeriveName);
							taskMxSQL.setOnSucceeded(new EventHandler<Event>() {
								@Override
								public void handle(Event arg0) {
									setButtonImage(bI7, imageViewOn);
								}
							});
							new Thread(taskMxSQL).start();
						}
						else {
							System.out.println("MySQL Dienst ist bereits gestartet");
						}
						if(!gestartetIntegral7) {
							System.out.println("Starte Integral 7 ");
							Utils.startProcess(integral7Path + integral7Exe);
							gestartetIntegral7 = Utils.isProcessRunning(integral7Exe);
							if(gestartetMySQL)
								System.out.println("Integral 7 gestartet");
						}
						else {
							System.out.println("Integral 7 ist bereits gestartet");
						}
						if(gestartetMySQL && gestartetIntegral7) {
							setButtonImage(bI7, imageViewOn);
						}
						start = false;
					}	
					else {
						if(gestartetMySQL) {
							TaskMySQLService taskMxSQL = new TaskMySQLService(start, mySQLSeriveName);
							taskMxSQL.setOnSucceeded(new EventHandler<Event>() {
								@Override
								public void handle(Event arg0) {
									setButtonImage(bI7, imageViewOff);
								}
							});
							new Thread(taskMxSQL).start();
						}
						if(gestartetIntegral7) {
							System.out.println("Beende Integral 7 ");
							Utils.killProcess(integral7Exe);
							gestartetIntegral7 = Utils.isProcessRunning(integral7Exe);
						}
						if(!gestartetMySQL && !gestartetIntegral7) {
							setButtonImage(bI7, imageViewOff);
						}
						start = true;
					}
				}
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void initProperties() {
		if(!Utils.getCurrentPath().contains("workspace")){
			integral7Exe = Utils.getPropertyValue(PROP_I7_EXE);
			integral7Path = Utils.getPropertyValue(PROP_I7_DIR);
			integral7UpdatePath = Utils.getPropertyValue(PROP_UPDATE_DIR);
			mySQLSeriveName = Utils.getPropertyValue(PROP_MY_SQL_SERVICE);
		}
	}

	private void setButtonImage(Button btn, ImageView imageView) {
		btn.setGraphic(imageView);
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	private boolean isRunning() {
		return Utils.isRunningMySQL(mySQLSeriveName) && Utils.isProcessRunning(integral7Exe);
	}
	
	
	
	@Override
	public void stop(){
		if(!start)
			bI7.fire();
	}
}
