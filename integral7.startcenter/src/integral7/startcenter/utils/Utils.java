package integral7.startcenter.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Utils {

	
	private static final String TASKLIST = "tasklist";
	private static final String KILL = "taskkill /F /IM ";
	
	
	public static synchronized boolean  isRunningMySQL(String mysqlServicename) {
		String[] scriptIsRunning = {"cmd.exe", "/c", "sc", "query", mysqlServicename, "|", "find", "/C", "\"RUNNING\""};
		
		boolean isRunning = false;
		
        Runtime rt = Runtime.getRuntime();
        try {
			Process process = rt.exec(scriptIsRunning);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
	        
	        String s = null;
	        while ((s = stdInput.readLine()) != null) {
	        	try {
	        		int value =  Integer.valueOf(s);
	        		if(value == 1) {
	        			isRunning = true;
	        		}
	        	}
	        	catch(NumberFormatException e) {
	        		System.out.println(getStringStackTrace(e));
	        	} 
	    		catch (NullPointerException e) {
	    			System.out.println(getStringStackTrace(e));
	    		}
	    		catch (Exception e) {
	    			System.out.println(getStringStackTrace(e));
	    		}
	        }


		} catch (IOException e) {
			e.printStackTrace();
		}
               	
        return isRunning;
	}
	

	
	
	public static synchronized void startServiceMySQL() {
		List<String> commandsStartMySQL = Arrays.asList("schtasks.exe", "/RUN", "/I", "/TN", "ServiceStartMySQL");
		
		ProcessBuilder builder = new ProcessBuilder(commandsStartMySQL);
    	Process p;
		try {
			p = builder.start();
	    	p.waitFor();  
	    	
	    	BufferedReader stdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
	    	print(stdOut);
	    	BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
	    	print(stdError);    	
	    	System.out.println(p.exitValue());
	    	
		} catch (IOException e) {
			System.out.println(getStringStackTrace(e));
		} catch (InterruptedException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (NullPointerException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (Exception e) {
			System.out.println(getStringStackTrace(e));
		}
	}
	
	public static synchronized void stopServiceMySQL() {
		List<String> commandsStopMySQL = Arrays.asList("schtasks.exe", "/RUN", "/I", "/TN", "ServiceStopMySQL");
		
		ProcessBuilder builder = new ProcessBuilder(commandsStopMySQL);
    	Process p;
		try {
			p = builder.start();
	    	p.waitFor();  
	    	
	    	BufferedReader stdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
	    	print(stdOut);
	    	BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
	    	print(stdError);    	
	    	System.out.println(p.exitValue());
	    	
		} catch (IOException e) {
			System.out.println(getStringStackTrace(e));
		} catch (InterruptedException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (NullPointerException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (Exception e) {
			System.out.println(getStringStackTrace(e));
		}
	}

	
	public static synchronized void killProcess(String executable) {
		try {
//			Process p = new ProcessBuilder(KILL + executable).start();
			Runtime.getRuntime().exec(KILL + executable);
			
		} catch (IOException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (NullPointerException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (Exception e) {
			System.out.println(getStringStackTrace(e));
		}
	}
	
	public static synchronized void startProcess(String exe) {
		try {
			Process p = new ProcessBuilder(exe).start();
//	    	BufferedReader stdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
//	    	print(stdOut);
//	    	BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
//	    	print(stdError);    	
//	    	System.out.println(p.exitValue());
		} catch (IOException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (NullPointerException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (Exception e) {
			System.out.println(getStringStackTrace(e));
		}
		
	}
	
	public static boolean isProcessRunning(String serviceName) {
		Process p;
		try {
			p = Runtime.getRuntime().exec(TASKLIST);
			 BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			 String line;
			 while ((line = reader.readLine()) != null) {
				 if (line.contains(serviceName)) {
					 
					 return true;
				 }
			 }
		} catch (IOException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (NullPointerException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (Exception e) {
			System.out.println(getStringStackTrace(e));
		}

	 return false;
	}
	
	private static void print(BufferedReader reader) {
		String s = null;
		try {
			while ((s = reader.readLine()) != null) {
			    System.out.println(s);
			}
			reader.close();
		} catch (IOException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (NullPointerException e) {
			System.out.println(getStringStackTrace(e));
		}
		catch (Exception e) {
			System.out.println(getStringStackTrace(e));
		}
	}
	
	
	public static String getStringStackTrace(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		
		String exc =  sw.toString();
		pw.close();
		try {sw.close();} catch (IOException e1) {}
		sw = null;
		pw = null;
		return exc; 
	}
	
	public static String getPropertyValue(String propName) {
		String prop = "";
		
		String rootPath = getCurrentPath();
		Path path = Paths.get(rootPath + "/properties");
		Charset charset = StandardCharsets.UTF_8;
		try {
			List<String> lines = Files.readAllLines(path, charset);
			for (int i = 0; i < lines.size(); i++) {
				String[] line = lines.get(i).split("=");
				
				if(line.length == 2 && line[0].equals(propName)) {
					prop = line[1];
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return prop;
	}
	
	public static String getCurrentPath() {
		  final String dir = System.getProperty("user.dir");
		  return dir;
	}
}
